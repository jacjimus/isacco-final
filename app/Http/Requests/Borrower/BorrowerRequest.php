<?php

namespace App\Http\Requests\Borrower;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Foundation\Http\FormRequest;

class BorrowerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
      $this->merge(['user_id' => Sentinel::getUser()->id]);
    }

  /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'first_name' =>['required', 'string'],
          'user_id' =>['required' , 'integer'],
          'last_name' =>['required', 'string'],
          'gender' => ['string'],
          'title' =>['string'],
          'mobile' =>['required' , 'unique:borrowers'],
          'notes' =>['nullable'],
          'email' =>['email'],
          'unique_number' => ['required'],
          'id_no' => ['required' , 'unique:borrowers'],
          'address' => ['nullable'],
          'city' => ['nullable'],
          'state' => ['nullable'],
          'zip' => ['nullable'],
          'phone' => ['nullable'],
          'business_name' => ['nullable'],
           'working_status' => ['nullable'],
        ];
    }
}
