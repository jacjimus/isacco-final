<?php

use App\Models\SavingTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('validate', function(Request $request){
  if($request->secret == getenv('VALIDATION_CODE')) {
    //Log::info($request->getContent());
    echo response()->json([
      'ResultCode' => 0,
      'ResultDesc' => 'Success',
      'ThirdPartyTransID' => 0
    ]);

    //insert transaction
  }
});
/*
 * M-PESA payment confirmation url
 */
Route::post('confirmation', function(Request $request){

  $log = true;

    $callbackJSONData=file_get_contents('php://input');
    if(!$callbackJSONData){
      return response('ERROR: NO REQUEST');
    }

    $callbackData=json_decode($callbackJSONData );
      dump($callbackData);

    try {
      $resultCode = $callbackData->Body->stkCallback->ResultCode;
      $checkoutRequestID = $callbackData->Body->stkCallback->CheckoutRequestID;

      $sub = Checkouts::where('checkout_request_id', $checkoutRequestID)->first();


      if ($resultCode == '0') {

        $amount = $callbackData->Body->stkCallback->CallbackMetadata->Item[0]->Value;
        $receipt = $callbackData->Body->stkCallback->CallbackMetadata->Item[1]->Value;
        $trans_date = $callbackData->Body->stkCallback->CallbackMetadata->Item[2]->Value;
        $phone = $callbackData->Body->stkCallback->CallbackMetadata->Item[3]->Value;

        Checkouts::where('checkout_request_id', $checkoutRequestID)->update(['status' => 'Paid']);

        /*
         * Insert the transaction details
         */

        $data = ['transaction_reference' => $receipt,
                 'transaction_date' => date('Y-m-d', strtotime($trans_date)),
                 'transaction_time' => date('H:i:s', strtotime($trans_date)),
                 'account_number' => $sub->count() > 0 ? $sub->subscription->account_code : $phone,
                 'sender_phone' => $phone,
                 'amount' => $amount,
                 'status' => 'New',
        ];

        Transactions::insert($data);
        if ($sub->count() > 0) {
          if ($sub->count() > 0)
            $other_payments = \App\Models\Transactions::where('account_number', $sub->subscription->account_code)->where('status', 'New')->sum('amount');
          else
            $other_payments = 0;
          $update['transaction_id'] = DB::getPdo()->lastInsertId();

          if (($amount + $other_payments) >= \App\Models\Bundles::where('bundle', $sub->subscription->bundle)->first()->amount)
            $update['status'] = 'Active';
          if ($sub->count() > 0) {
            Subscriptions::find($sub->subscription_id)->update($update);
            SavingTransaction::where('account_number', $sub->subscription->account_code)->orderBy('transaction_date', 'DESC')->limit(1)->update(['status' => 'New']);
          }
        }

      }
      echo response()->json([
        'ResultCode' => 0,
        'ResultDesc' => 'Confirmation received successfully'
      ],200);
    }
    catch (Exception $e){
      echo response()->json([
        'ResultCode' => $e->getCode(),
        'ResultDesc' => $e->getMessage()
      ], 201);
    }

});

