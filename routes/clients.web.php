<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('client_dashboard', [ClientController::class, 'clientDashboard']);
Route::get('client_profile', [ClientController::class, 'clientProfile']);
Route::post('client_register', [ClientController::class, 'processClientRegister']);
Route::post('client_profile', [ClientController::class, 'processClientProfile']);
Route::get('mpesa', [ClientController::class, 'mpesa']);
Route::prefix('client')->group(function () {
  Route::get('register', [HomeController::class, 'clientRegister']);
  Route::post('register', [HomeController::class, 'processClientRegister']);
  Route::get('application/data', [ClientController::class, 'indexApplication']);
  Route::get('application/create', [ClientController::class, 'createApplication']);
  Route::get('application/{loan_application}/show', [ClientController::class, 'showApplication']);
  Route::get('application/{loan_application}/guarantor/create', [ClientController::class, 'createGuarantor']);
  Route::post('application/{loan_application}/guarantor/store', [ClientController::class, 'storeGuarantor']);
  Route::post('application/store', [ClientController::class, 'storeApplication']);
  Route::get('guarantor/data', [ClientController::class, 'indexGuarantor']);
  Route::get('guarantor/{id}/decline', [ClientController::class, 'declineGuarantor']);
  Route::post('guarantor/{id}/accept', [ClientController::class, 'acceptGuarantor']);
  Route::get('loan/{loan}/show', [ClientController::class, 'showLoan']);
  Route::get('loan/{loan}/pay', [ClientController::class, 'pay']);
  Route::post('loan/{loan}/pay/paynow', [ClientController::class, 'paynow']);
  Route::post('loan/{loan}/pay/stripe', [ClientController::class, 'stripe']);
  Route::any('loan/{loan}/pay/paynow/return', [ClientController::class, 'paynowReturn']);
  Route::any('loan/{loan}/pay/paynow/result', [ClientController::class, 'paynowResult']);
  Route::any('loan/{loan}/pay/paypal/done', [ClientController::class, 'paypalDone']);
  Route::any('loan/pay/paypal/ipn', [ClientController::class, 'paypalIPN']);
  Route::get('saving/data', [ClientController::class, 'indexSaving']);
  Route::get('saving/{saving}/show', [ClientController::class, 'showSaving']);
  Route::get('saving/{saving}/statement/print', [ClientController::class, 'printSavingStatement']);
  Route::get('saving/{saving}/statement/pdf', [ClientController::class, 'pdfSavingStatement']);
  Route::get('saving/{saving}/pay', [ClientController::class, 'paySaving']);
  Route::post('saving/{saving}/pay/paynow', [ClientController::class, 'paynowSaving']);
  Route::post('saving/{saving}/pay/stripe', [ClientController::class, 'stripeSaving']);
  Route::any('saving/{saving}/pay/paynow/return', [ClientController::class, 'paynowReturnSaving']);
  Route::any('saving/{saving}/pay/paynow/result', [ClientController::class, 'paynowResultSaving']);
  Route::any('saving/pay/paypal/done', [ClientController::class, 'paypalDoneSaving']);
  Route::any('saving/pay/paypal/ipn', [ClientController::class, 'paypalIPNSaving']);
  //mpesa
  Route::post('loan/{loan}/pay/mpesa', [ClientController::class, 'mpesa']);
  Route::get('loan/mpesa/confirm', [ClientController::class, 'confirm_mpesa_loan']);
  Route::get('loan/mpesa/validate', [ClientController::class, 'validate_mpesa_loan']);
  Route::post('saving/{saving}/pay/mpesa', [ClientController::class, 'mpesa_saving']);
  Route::get('saving/mpesa/confirm', [ClientController::class, 'confirm_mpesa_saving']);
  Route::get('saving/mpesa/validate', [ClientController::class, 'validate_mpesa_saving']);
  //statements
  Route::get('loan/{loan}/loan_statement/print', [ClientController::class, 'printLoanStatement']);
  Route::get('loan/{loan}/loan_statement/pdf', [ClientController::class, 'pdfLoanStatement']);
  Route::get('loan/{loan}/loan_statement/email', [ClientController::class, 'emailLoanStatement']);
  Route::get('loan/{borrower}/borrower_statement/print', [ClientController::class, 'printBorrowerStatement']);
  Route::get('loan/{borrower}/borrower_statement/pdf', [ClientController::class, 'pdfBorrowerStatement']);
  Route::get('loan/{borrower}/borrower_statement/email', [ClientController::class, 'emailBorrowerStatement']);
  Route::get('loan/{loan}/schedule/print', [ClientController::class, 'printSchedule']);
  Route::get('loan/{loan}/schedule/pdf', [ClientController::class, 'pdfSchedule']);
});
