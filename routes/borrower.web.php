<?php

use App\Http\Controllers\BorrowerController;
use App\Http\Controllers\BorrowerGroupController;
use Illuminate\Support\Facades\Route;

Route::prefix('borrower')->group(function () {

  Route::get('data', [BorrowerController::class, 'index']);
  Route::get('pending', [BorrowerController::class, 'pending']);
  Route::get('create', [BorrowerController::class, 'create']);
  Route::post('store', [BorrowerController::class, 'store']);
  Route::get('{borrower}/show', [BorrowerController::class, 'show']);
  Route::get('{borrower}/edit', [BorrowerController::class, 'edit']);
  Route::post('{id}/update', [BorrowerController::class, 'update']);
  Route::get('{id}/delete', [BorrowerController::class, 'delete']);
  Route::get('{id}/approve', [BorrowerController::class, 'approve']);
  Route::get('{id}/decline', [BorrowerController::class, 'decline']);
  Route::get('{id}/delete_file', [BorrowerController::class, 'deleteFile']);
  Route::get('{id}/blacklist', [BorrowerController::class, 'blacklist']);
  Route::get('{id}/unblacklist', [BorrowerController::class, 'unBlacklist']);
  //borrower group
  Route::get('group/data', [BorrowerGroupController::class, 'index']);
  Route::get('group/create', [BorrowerGroupController::class, 'create']);
  Route::post('group/store', [BorrowerGroupController::class, 'store']);
  Route::get('group/{borrower_group}/show', [BorrowerGroupController::class, 'show']);
  Route::get('group/{borrower_group}/edit', [BorrowerGroupController::class, 'edit']);
  Route::post('group/{id}/update', [BorrowerGroupController::class, 'update']);
  Route::get('group/{id}/delete', [BorrowerGroupController::class, 'delete']);
  Route::post('group/{id}/add_borrower', [BorrowerGroupController::class, 'addBorrower']);
  Route::get('group/{id}/remove_borrower', [BorrowerGroupController::class, 'removeBorrower']);
});
