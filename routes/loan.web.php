<?php

use App\Http\Controllers\GuarantorController;
use App\Http\Controllers\LoanCommentController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\LoanDisbursedByController;
use App\Http\Controllers\LoanFeeController;
use App\Http\Controllers\LoanOverduePenaltyController;
use App\Http\Controllers\LoanProductController;
use App\Http\Controllers\LoanRepaymentMethodController;
use App\Http\Controllers\LoanStatusController;
use App\Http\Controllers\ProvisionRateController;
use Illuminate\Support\Facades\Route;

Route::prefix(' loan')->group(function () {
  //main loan routes
  Route::get('data', [LoanController::class, 'index']);
  Route::get('pending_approval', [LoanController::class, 'pendingApproval']);
  Route::get('pending_disbursement', [LoanController::class, 'pendingDisbursement']);
  Route::get('declined', [LoanController::class, 'declined']);
  Route::get('withdrawn', [LoanController::class, 'withdrawn']);
  Route::get('written_off', [LoanController::class, 'writtenOff']);
  Route::get('closed', [LoanController::class, 'closed']);
  Route::get('pending_reschedule', [LoanController::class, 'pendingReschedule']);
  Route::get('{id}/reschedule', [LoanController::class, 'reschedule']);
  Route::post('{id}/reschedule/store', [LoanController::class, 'rescheduleStore']);
  Route::post('{id}/approve', [LoanController::class, 'approve']);
  Route::get('{id}/unapprove', [LoanController::class, 'unapprove']);
  Route::post('{id}/decline', [LoanController::class, 'decline']);
  Route::post('{loan}/disburse', [LoanController::class, 'disburse']);
  Route::get('{id}/undisburse', [LoanController::class, 'undisburse']);
  Route::post('{id}/withdraw', [LoanController::class, 'withdraw']);
  Route::post('{id}/write_off', [LoanController::class, 'write_off']);
  Route::post('{id}/reschedule', [LoanController::class, 'reschedule']);
  Route::get('{id}/unwithdraw', [LoanController::class, 'unwithdraw']);
  Route::get('{id}/unwrite_off', [LoanController::class, 'unwrite_off']);
  Route::post('{loan}/waive_interest', [LoanController::class, 'waiveInterest']);
  Route::post('{loan}/add_charge', [LoanController::class, 'addCharge']);
  Route::get('create', [LoanController::class, 'create']);
  Route::post('store', [LoanController::class, 'store']);
  Route::get('{loan}/edit', [LoanController::class, 'edit']);
  Route::get('{loan}/show', [LoanController::class, 'show']);
  Route::any('{loan}/override', [LoanController::class, 'override']);
  Route::post('{id}/update', [LoanController::class, 'update']);
  Route::get('{id}/delete', [LoanController::class, 'delete']);
  Route::get('{id}/delete_file', [LoanController::class, 'deleteFile']);
  Route::get('{loan}/loan_statement/print', [LoanController::class, 'printLoanStatement']);
  Route::get('{loan}/loan_statement/pdf', [LoanController::class, 'pdfLoanStatement']);
  Route::get('{loan}/loan_statement/email', [LoanController::class, 'emailLoanStatement']);
  Route::get('{borrower}/borrower_statement/print', [LoanController::class, 'printBorrowerStatement']);
  Route::get('{borrower}/borrower_statement/pdf', [LoanController::class, 'pdfBorrowerStatement']);
  Route::get('{borrower}/borrower_statement/email', [LoanController::class, 'emailBorrowerStatement']);
  //loan repayment routes
  Route::get('repayment/{id}/reverse', [LoanController::class, 'reverseRepayment']);
  Route::get('{loan}/repayment/data', [LoanController::class, 'indexRepayment']);
  Route::get('{loan}/repayment/create', [LoanController::class, 'createRepayment']);
  Route::get('repayment/create', [LoanController::class, 'addRepayment']);
  Route::post('{loan}/repayment/store', [LoanController::class, 'storeRepayment']);
  Route::get('repayment/{loan_transaction}/edit', [LoanController::class, 'editRepayment']);
  Route::get('repayment/show', [LoanController::class, 'showRepayment']);
  Route::post('repayment/{id}/update', [LoanController::class, 'updateRepayment']);
  Route::get('repayment/{loan_transaction}/pdf', [LoanController::class, 'pdfRepayment']);
  Route::get('repayment/{loan_transaction}/email', [LoanController::class, 'emailRepayment']);
  Route::get('repayment/{loan_transaction}/print', [LoanController::class, 'printRepayment']);
  //transactions
  Route::get('transaction/{loan_transaction}/show', [LoanController::class, 'showTransaction']);
  Route::get('transaction/{id}/waive', [LoanController::class, 'waiveTransaction']);
  Route::get('transaction/{loan_transaction}/print', [LoanController::class, 'printTransaction']);
  Route::get('transaction/{loan_transaction}/pdf', [LoanController::class, 'pdfTransaction']);
  Route::get('transaction/{loan_transaction}/email', [LoanController::class, 'emailTransaction']);
  //comment routes
  Route::get('loan_comment/data', [LoanCommentController::class, 'index']);
  Route::get('{id}/loan_comment/create', [LoanCommentController::class, 'create']);
  Route::post('{id}/loan_comment/store', [LoanCommentController::class, 'store']);
  Route::get('{id}/loan_comment/{loan_comment}/edit', [LoanCommentController::class, 'edit']);
  Route::get('{id}/loan_comment/{loan_comment}/show', [LoanCommentController::class, 'show']);
  Route::post('{id}/loan_comment/{cid}/update', [LoanCommentController::class, 'update']);
  Route::get('{id}/loan_comment/{cid}/delete', [LoanCommentController::class, 'delete']);
  //status routes
  Route::get('loan_status/data', [LoanStatusController::class, '  index']);
  Route::get('loan_status/create', [LoanStatusController::class, '  create']);
  Route::post('loan_status/store', [LoanStatusController::class, '  store']);
  Route::get('loan_status/{loan_status}/edit', [LoanStatusController::class, '  edit']);
  Route::get('loan_status/{loan_status}/show', [LoanStatusController::class, '  show']);
  Route::post('loan_status/{id}/update', [LoanStatusController::class, '  update']);
  Route::get('loan_status/{id}/delete', [LoanStatusController::class, '  delete']);
  //routes for disbursed by
  Route::get('loan_disbursed_by/data', [LoanDisbursedByController::class, 'index']);
  Route::get('loan_disbursed_by/create', [LoanDisbursedByController::class, 'create']);
  Route::post('loan_disbursed_by/store', [LoanDisbursedByController::class, 'store']);
  Route::get('loan_disbursed_by/{loan_disbursed_by}/edit', [LoanDisbursedByController::class, 'edit']);
  Route::get('loan_disbursed_by/{loan_disbursed_by}/show', [LoanDisbursedByController::class, 'show']);
  Route::post('loan_disbursed_by/{id}/update', [LoanDisbursedByController::class, 'update']);
  Route::get('loan_disbursed_by/{id}/delete', [LoanDisbursedByController::class, 'delete']);
  //routes for repayment method
  Route::get('loan_repayment_method/data', [LoanRepaymentMethodController::class, 'index']);
  Route::get('loan_repayment_method/create', [LoanRepaymentMethodController::class, 'create']);
  Route::post('loan_repayment_method/store', [LoanRepaymentMethodController::class, 'store']);
  Route::get('loan_repayment_method/{loan_repayment_method}/edit', [LoanRepaymentMethodController::class, 'edit']);
  Route::get('loan_repayment_method/{loan_repayment_method}/show', [LoanRepaymentMethodController::class, 'show']);
  Route::post('loan_repayment_method/{id}/update', [LoanRepaymentMethodController::class, 'update']);
  Route::get('loan_repayment_method/{id}/delete', [LoanRepaymentMethodController::class, 'delete']);
  //routes for loan product
  Route::get('loan_product/data', [LoanProductController::class, 'index']);
  Route::get('loan_product/create', [LoanProductController::class, 'create']);
  Route::post('loan_product/store', [LoanProductController::class, 'store']);
  Route::get('loan_product/{loan_product}/edit', [LoanProductController::class, 'edit']);
  Route::get('loan_product/{loan_product}/show', [LoanProductController::class, 'show']);
  Route::get('loan_product/get_charge_detail/{charge}', [LoanProductController::class, 'get_charge_detail']);
  Route::post('loan_product/{id}/update', [LoanProductController::class, 'update']);
  Route::get('loan_product/{id}/delete', [LoanProductController::class, 'delete']);
  //route for managing schedules
  Route::get('{loan}/schedule/edit', [LoanController::class, 'editSchedule']);
  Route::post('{loan}/schedule/update', [LoanController::class, 'updateSchedule']);
  Route::get('{loan}/schedule/print', [LoanController::class, 'printSchedule']);
  Route::get('{loan}/schedule/pdf', [LoanController::class, 'pdfSchedule']);
  Route::get('{loan}/schedule/email', [LoanController::class, 'emailLoanSchedule']);
  //routes for repayment method
  Route::get('loan_fee/data', [LoanFeeController::class, 'index']);
  Route::get('loan_fee/create', [LoanFeeController::class, 'create']);
  Route::post('loan_fee/store', [LoanFeeController::class, 'store']);
  Route::get('loan_fee/{loan_fee}/edit', [LoanFeeController::class, 'edit']);
  Route::get('loan_fee/{loan_fee}/show', [LoanFeeController::class, 'show']);
  Route::post('loan_fee/{id}/update', [LoanFeeController::class, 'update']);
  Route::get('loan_fee/{id}/delete', [LoanFeeController::class, 'delete']);
  //routes for repayment method
  Route::get('loan_overdue_penalty/data', [LoanOverduePenaltyController::class, 'index']);
  Route::get('loan_overdue_penalty/create', [LoanOverduePenaltyController::class, 'create']);
  Route::post('loan_overdue_penalty/store', [LoanOverduePenaltyController::class, 'store']);
  Route::get('loan_overdue_penalty/{loan_overdue_penalty}/edit', [LoanOverduePenaltyController::class, 'edit']);
  Route::get('loan_overdue_penalty/{loan_overdue_penalty}/show', [LoanOverduePenaltyController::class, 'show']);
  Route::post('loan_overdue_penalty/{id}/update', [LoanOverduePenaltyController::class, 'update']);
  Route::get('loan_overdue_penalty/{id}/delete', [LoanOverduePenaltyController::class, 'delete']);
  //routes for applications
  Route::get('loan_application/data', [LoanController::class, 'indexApplication']);
  Route::get('loan_application/{id}/decline', [LoanController::class, 'declineApplication']);
  Route::get('loan_application/{id}/approve', [LoanController::class, 'approveApplication']);
  Route::post('loan_application/{id}/store', [LoanController::class, 'storeApproveApplication']);
  //routes for guarantors
  Route::get('{loan}/guarantor/data', [GuarantorController::class, 'index']);
  Route::post('{loan}/guarantor/add', [LoanController::class, 'add_guarantor']);
  Route::get('guarantor/{id}/remove', [LoanController::class, 'remove_guarantor']);

  //loan calculator
  Route::get('loan_calculator/create', [LoanController::class, 'createLoanCalculator']);
  Route::post('loan_calculator/show', [LoanController::class, 'showLoanCalculator']);
  Route::post('loan_calculator/store', [LoanController::class, 'storeLoanCalculator']);
  //routes for provision rates
  Route::get('provision/data', [ProvisionRateController::class, 'index']);
  Route::get('provision/create', [ProvisionRateController::class, 'create']);
  Route::post('provision/store', [ProvisionRateController::class, 'store']);
  Route::get('provision/{provision}/edit', [ProvisionRateController::class, 'edit']);
  Route::get('provision/{provision}/show', [ProvisionRateController::class, 'show']);
  Route::post('provision/{id}/update', [ProvisionRateController::class, 'update']);
  Route::get('provision/{id}/delete', [ProvisionRateController::class, 'delete']);
});
