<?php

use App\Http\Controllers\OtherIncomeController;
use Illuminate\Support\Facades\Route;

Route::prefix('other_income')->group(function () {
  Route::get('data', [OtherIncomeController::class, 'index']);
  Route::get('create', [OtherIncomeController::class, 'create']);
  Route::post('store', [OtherIncomeController::class, 'store']);
  Route::get('{other_income}/edit', [OtherIncomeController::class, 'edit']);
  Route::get('{other_income}/show', [OtherIncomeController::class, 'show']);
  Route::post('{id}/update', [OtherIncomeController::class, 'update']);
  Route::get('{id}/delete', [OtherIncomeController::class, 'delete']);
  Route::get('{id}/delete_file', [OtherIncomeController::class, 'deleteFile']);
  //income types
  Route::get('type/data', [OtherIncomeController::class, 'indexType']);
  Route::get('type/create', [OtherIncomeController::class, 'createType']);
  Route::post('type/store', [OtherIncomeController::class, 'storeType']);
  Route::get('type/{other_income_type}/edit', [OtherIncomeController::class, 'editType']);
  Route::get('type/{other_income_type}/show', [OtherIncomeController::class, 'showType']);
  Route::post('type/{id}/update', [OtherIncomeController::class, 'updateType']);
  Route::get('type/{id}/delete', [OtherIncomeController::class, 'deleteType']);
});
