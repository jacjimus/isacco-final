<?php

use App\Http\Controllers\ChargeController;
use Illuminate\Support\Facades\Route;

Route::prefix('charge')->group(function () {

  Route::get('data', [ChargeController::class, 'index']);
  Route::get('create', [ChargeController::class, 'create']);
  Route::post('store', [ChargeController::class, 'store']);
  Route::get('{charge}/show', [ChargeController::class, 'show']);
  Route::get('{charge}/edit', [ChargeController::class, 'edit']);
  Route::post('{id}/update', [ChargeController::class, 'update']);
  Route::get('{id}/delete', [ChargeController::class, 'delete']);

});
