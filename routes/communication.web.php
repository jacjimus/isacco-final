<?php

use App\Http\Controllers\CommunicationController;
use Illuminate\Support\Facades\Route;

Route::prefix('communication')->group(function () {
  Route::get('email', [CommunicationController::class, 'indexEmail']);
  Route::get('sms', [CommunicationController::class, 'indexSms']);
  Route::get('email/create', [CommunicationController::class, 'createEmail']);
  Route::post('email/store', [CommunicationController::class, 'storeEmail']);
  Route::get('email/{id}/delete', [CommunicationController::class, 'deleteEmail']);
  Route::get('sms/create', [CommunicationController::class, 'createSms']);
  Route::post('sms/store', [CommunicationController::class, 'storeSms']);
  Route::get('sms/{id}/delete', [CommunicationController::class, 'deleteSms']);

});
