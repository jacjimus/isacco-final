<?php

use App\Http\Controllers\ExpenseController;
use Illuminate\Support\Facades\Route;

Route::prefix('expense')->group(function () {
  Route::get('data', [ExpenseController::class, 'index']);
  Route::get('create', [ExpenseController::class, 'create']);
  Route::post('store', [ExpenseController::class, 'store']);
  Route::get('{expense}/edit', [ExpenseController::class, 'edit']);
  Route::get('{expense}/show', [ExpenseController::class, 'show']);
  Route::post('{id}/update', [ExpenseController::class, 'update']);
  Route::get('{id}/delete', [ExpenseController::class, 'delete']);
  Route::get('{id}/delete_file', [ExpenseController::class, 'deleteFile']);

  //expense types
  Route::get('type/data', [ExpenseController::class, 'indexType']);
  Route::get('type/create', [ExpenseController::class, 'createType']);
  Route::post('type/store', [ExpenseController::class, 'storeType']);
  Route::get('type/{expense_type}/edit', [ExpenseController::class, 'editType']);
  Route::get('type/{expense_type}/show', [ExpenseController::class, 'showType']);
  Route::post('type/{id}/update', [ExpenseController::class, 'updateType']);
  Route::get('type/{id}/delete', [ExpenseController::class, 'deleteType']);
});
