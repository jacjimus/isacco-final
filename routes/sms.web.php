<?php

use App\Http\Controllers\SmsGatewayController;
use Illuminate\Support\Facades\Route;

Route::prefix('sms_gateway')->group(function () {

  Route::get('data', [SmsGatewayController::class, 'index']);
  Route::get('create', [SmsGatewayController::class, 'create']);
  Route::post('store', [SmsGatewayController::class, 'store']);
  Route::get('{sms_gateway}/show', [SmsGatewayController::class, 'show']);
  Route::get('{sms_gateway}/edit', [SmsGatewayController::class, 'edit']);
  Route::post('{id}/update', [SmsGatewayController::class, 'update']);
  Route::get('{id}/delete', [SmsGatewayController::class, 'delete']);
});
