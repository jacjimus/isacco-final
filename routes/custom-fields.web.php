<?php

use App\Http\Controllers\CustomFieldController;
use Illuminate\Support\Facades\Route;

Route::prefix('custom_field')->group(function () {

  Route::get('data', [CustomFieldController::class, 'index']);
  Route::get('create', [CustomFieldController::class, 'create']);
  Route::post('store', [CustomFieldController::class, 'store']);
  Route::get('{custom_field}/show', [CustomFieldController::class, 'show']);
  Route::get('{custom_field}/edit', [CustomFieldController::class, 'edit']);
  Route::post('{id}/update', [CustomFieldController::class, 'update']);
  Route::get('{id}/delete', [CustomFieldController::class, 'delete']);

});
