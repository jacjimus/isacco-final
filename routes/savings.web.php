<?php

use App\Http\Controllers\SavingController;
use App\Http\Controllers\SavingFeeController;
use App\Http\Controllers\SavingProductController;
use App\Http\Controllers\SavingTransactionController;
use Illuminate\Support\Facades\Route;

Route::prefix('saving')->group(function () {
  Route::get('data', [SavingController::class, 'index']);
  Route::get('create', [SavingController::class, 'create']);
  Route::post('store', [SavingController::class, 'store']);
  Route::get('{saving}/edit', [SavingController::class, 'edit']);
  Route::get('{saving}/show', [SavingController::class, 'show']);
  Route::post('{id}/update', [SavingController::class, 'update']);
  Route::get('{id}/delete', [SavingController::class, 'delete']);
  Route::get('{saving}/statement/print', [SavingController::class, 'printStatement']);
  Route::get('{saving}/statement/pdf', [SavingController::class, 'pdfStatement']);
  Route::get('{saving}/transfer/create', [SavingController::class, 'transfer']);
  Route::post('{saving}/transfer/store', [SavingController::class, 'storeTransfer']);
  //saving products
  Route::get('savings_product/data', [SavingProductController::class, 'index']);
  Route::get('savings_product/create', [SavingProductController::class, 'create']);
  Route::post('savings_product/store', [SavingProductController::class, 'store']);
  Route::get('savings_product/{savings_product}/edit', [SavingProductController::class, 'edit']);
  Route::post('savings_product/{id}/update', [SavingProductController::class, 'update']);
  Route::get('savings_product/{id}/delete', [SavingProductController::class, 'delete']);
  //saving fees
  Route::get('savings_fee/data', [SavingFeeController::class, 'index']);
  Route::get('savings_fee/create', [SavingFeeController::class, 'create']);
  Route::post('savings_fee/store', [SavingFeeController::class, 'store']);
  Route::get('savings_fee/{savings_fee}/edit', [SavingFeeController::class, 'edit']);
  Route::post('savings_fee/{id}/update', [SavingFeeController::class, 'update']);
  Route::get('savings_fee/{id}/delete', [SavingFeeController::class, 'delete']);
  //saving transactions
  Route::get('savings_transaction/data', [SavingTransactionController::class, 'index']);
  Route::get('{saving}/savings_transaction/create', [SavingTransactionController::class, 'create']);
  Route::post('{saving}/savings_transaction/store', [SavingTransactionController::class, 'store']);
  Route::get('savings_transaction/{savings_transaction}/edit', [SavingTransactionController::class, 'edit']);
  Route::get('savings_transaction/{savings_transaction}/show', [SavingTransactionController::class, 'show']);
  Route::post('savings_transaction/{id}/update', [SavingTransactionController::class, 'update']);
  Route::get('savings_transaction/{id}/delete', [SavingTransactionController::class, 'delete']);
  Route::get('savings_transaction/{savings_transaction}/pdf', [SavingTransactionController::class, 'pdf_transaction']);
  Route::get('savings_transaction/{savings_transaction}/email', [SavingTransactionController::class, 'email_transaction']);
  Route::get('savings_transaction/{savings_transaction}/print', [SavingTransactionController::class, 'print_transaction']);
  Route::get('savings_transaction/{id}/reverse', [SavingTransactionController::class, 'reverse']);
});
