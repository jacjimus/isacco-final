<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class , 'index']);
Route::get('error', [HomeController::class , 'error']);
commonRoutes();

function commonRoutes(): void
{
  Route::get('login', [HomeController::class , 'login']);
  Route::get('client', [HomeController::class ,'clientLogin']);
  Route::post('client', [HomeController::class , 'processClientLogin']);
  Route::get('client_logout', [HomeController::class , 'clientLogout']);
  Route::get('admin', [HomeController::class , 'adminLogin']);

  Route::get('logout', [HomeController::class , 'logout']);
  Route::post('login', [HomeController::class , 'processLogin']);
  Route::post('register', [HomeController::class , 'register']);
  Route::post('reset', [HomeController::class , 'passwordReset']);
  Route::get('reset/{id}/{code}', [HomeController::class , 'confirmReset']);
  Route::post('reset/{id}/{code}', [HomeController::class , 'completeReset']);
  Route::get('check/{id}', [HomeController::class , 'checkStatus']);
}


