<?php

use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;

Route::prefix('setting')->group( function () {
  Route::get('data', [SettingController::class, 'index']);
  Route::post('update', [SettingController::class, 'update']);
  Route::get('update_system', [SettingController::class, 'updateSystem']);
});
