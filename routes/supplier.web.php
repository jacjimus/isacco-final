<?php

use App\Http\Controllers\SupplierController;
use Illuminate\Support\Facades\Route;

Route::prefix('supplier')->group(function () {

  Route::get('data', [SupplierController::class, 'index']);
  Route::get('create', [SupplierController::class, 'create']);
  Route::post('store', [SupplierController::class, 'store']);
  Route::get('{supplier}/show', [SupplierController::class, 'show']);
  Route::get('{supplier}/edit', [SupplierController::class, 'edit']);
  Route::post('{id}/update', [SupplierController::class, 'update']);
  Route::get('{id}/delete', [SupplierController::class, 'delete']);

});
