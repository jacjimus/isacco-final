<?php

use App\Http\Controllers\AccountingController;
use Illuminate\Support\Facades\Route;

Route::prefix('accounting')->group(function () {

	Route::any('trial_balance', [AccountingController::class, 'trial_balance']);
	Route::any('ledger', [AccountingController::class, 'ledger']);
	Route::any('journal', [AccountingController::class, 'journal']);
	Route::get('manual_entry/create', [AccountingController::class, 'create_manual_entry']);
	Route::post('manual_entry/store', [AccountingController::class, 'store_manual_entry']);
});
