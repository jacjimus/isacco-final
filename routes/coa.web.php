<?php

use App\Http\Controllers\ChartOfAccountController;
use Illuminate\Support\Facades\Route;

Route::prefix('chart_of_account')->group(function () {

  Route::get('data', [ChartOfAccountController::class, 'index']);
  Route::get('create', [ChartOfAccountController::class, 'create']);
  Route::post('store', [ChartOfAccountController::class, 'store']);
  Route::get('{chart_of_account}/show', [ChartOfAccountController::class, 'show']);
  Route::get('{chart_of_account}/edit', [ChartOfAccountController::class, 'edit']);
  Route::post('{id}/update', [ChartOfAccountController::class, 'update']);
  Route::get('{id}/delete', [ChartOfAccountController::class, 'delete']);

});
