<?php

use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::prefix('product')->group(function () {

  Route::get('data', [ProductController::class, 'index']);
  Route::get('create', [ProductController::class, 'create']);
  Route::post('store', [ProductController::class, 'store']);
  Route::get('{product}/show', [ProductController::class, 'show']);
  Route::get('{product}/edit', [ProductController::class, 'edit']);
  Route::post('{id}/update', [ProductController::class, 'update']);
  Route::get('{id}/delete', [ProductController::class, 'delete']);
//category routes
  Route::get('category/data', [ProductCategoryController::class, 'index']);
  Route::get('category/create', [ProductCategoryController::class, 'create']);
  Route::post('category/store', [ProductCategoryController::class, 'store']);
  Route::get('category/{product_category}/edit', [ProductCategoryController::class, 'edit']);
  Route::get('category/{product_category}/show', [ProductCategoryController::class, 'show']);
  Route::post('category/{id}/update', [ProductCategoryController::class, 'update']);
  Route::get('category/{id}/delete', [ProductCategoryController::class, 'delete']);
});
