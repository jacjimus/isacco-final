<?php

use App\Http\Controllers\BankAccountController;
use App\Http\Controllers\CapitalController;
use Illuminate\Support\Facades\Route;

Route::prefix('capital')->group(function () {
  Route::get('data', [CapitalController::class, 'index']);
  Route::get('create', [CapitalController::class, 'create']);
  Route::post('store', [CapitalController::class, 'store']);
  Route::get('{capital}/edit', [CapitalController::class, 'edit']);
  Route::get('{id}/show', [CapitalController::class, 'show']);
  Route::post('{id}/update', [CapitalController::class, 'update']);
  Route::get('{id}/delete', [CapitalController::class, 'delete']);
  //bank accounts
  Route::get('bank/data', [BankAccountController::class, 'index']);
  Route::get('bank/create', [BankAccountController::class, 'create']);
  Route::post('bank/store', [BankAccountController::class, 'store']);
  Route::get('bank/{bank}/edit', [BankAccountController::class, 'edit']);
  Route::get('bank/{id}/show', [BankAccountController::class, 'show']);
  Route::post('bank/{id}/update', [BankAccountController::class, 'update']);
  Route::get('bank/{id}/delete', [BankAccountController::class, 'delete']);
});
