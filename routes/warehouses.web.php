<?php

use App\Http\Controllers\WarehouseController;
use Illuminate\Support\Facades\Route;

Route::prefix('warehouse')->group(function () {

  Route::get('data', [WarehouseController::class, 'index']);
  Route::get('create', [WarehouseController::class, 'create']);
  Route::post('store', [WarehouseController::class, 'store']);
  Route::get('{warehouse}/show', [WarehouseController::class, 'show']);
  Route::get('{warehouse}/edit', [WarehouseController::class, 'edit']);
  Route::post('{id}/update', [WarehouseController::class, 'update']);
  Route::get('{id}/delete', [WarehouseController::class, 'delete']);

});
