<?php

use App\Http\Controllers\CollateralController;
use Illuminate\Support\Facades\Route;

Route::prefix('collateral')->group(function () {
  Route::get('data', [CollateralController::class, 'index']);
  Route::get('{id}/create', [CollateralController::class, 'create']);
  Route::post('{loan}/store', [CollateralController::class, 'store']);
  Route::get('{collateral}/edit', [CollateralController::class, 'edit']);
  Route::get('{collateral}/show', [CollateralController::class, 'show']);
  Route::post('{id}/update', [CollateralController::class, 'update']);
  Route::get('{id}/delete', [CollateralController::class, 'delete']);
  Route::get('{id}/delete_file', [CollateralController::class, 'deleteFile']);
  // types
  Route::get('type/data', [CollateralController::class, 'indexType']);
  Route::get('type/fix/create', [CollateralController::class, 'createType']);
  Route::post('type/fix/store', [CollateralController::class, 'storeType']);
  Route::get('type/{collateral_type}/edit', [CollateralController::class, 'editType']);
  Route::get('type/{collateral_type}/show', [CollateralController::class, 'showType']);
  Route::post('type/{id}/update', [CollateralController::class, 'updateType']);
  Route::get('type/{id}/delete', [CollateralController::class, 'deleteType']);
});
