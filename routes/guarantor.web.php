<?php

use App\Http\Controllers\GuarantorController;
use Illuminate\Support\Facades\Route;

Route::prefix('guarantor')->group(function () {

  Route::get('data', [GuarantorController::class, 'index']);
  Route::get('pending', [GuarantorController::class, 'pending']);
  Route::get('create', [GuarantorController::class, 'create']);
  Route::post('store', [GuarantorController::class, 'store']);
  Route::get('{guarantor}/show', [GuarantorController::class, 'show']);
  Route::get('{guarantor}/edit', [GuarantorController::class, 'edit']);
  Route::post('{id}/update', [GuarantorController::class, 'update']);
  Route::get('{id}/delete', [GuarantorController::class, 'delete']);

});
