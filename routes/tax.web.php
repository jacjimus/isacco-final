<?php

use App\Http\Controllers\TaxController;
use Illuminate\Support\Facades\Route;

Route::prefix('tax')->group(function () {
  Route::get('data', [TaxController::class, 'index']);
  Route::get('create', [TaxController::class, 'create']);
  Route::post('store', [TaxController::class, 'store']);
  Route::get('{tax}/edit', [TaxController::class, 'edit']);
  Route::get('{id}/show', [TaxController::class, 'show']);
  Route::post('{id}/update', [TaxController::class, 'update']);
  Route::get('{id}/delete', [TaxController::class, 'destroy']);
});
