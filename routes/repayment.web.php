<?php

use App\Http\Controllers\LoanController;
use Illuminate\Support\Facades\Route;

Route::prefix('repayment')->group(function () {
  Route::get('data', [LoanController::class, 'indexRepayment']);
  Route::get('create', [LoanController::class, 'addRepayment']);
  Route::get('bulk/create', [LoanController::class, 'createBulkRepayment']);
  Route::post('bulk/store', [LoanController::class, 'storeBulkRepayment']);
});
