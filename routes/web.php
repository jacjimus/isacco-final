<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Route;
//route model binding

  Route::model('custom_field', 'App\Models\CustomField');
  Route::model('borrower', 'App\Models\Borrower');
  Route::model('setting', 'App\Models\Setting');
  Route::model('status', 'App\Models\LoanStatus');
  Route::model('loan_comment', 'App\Models\LoanComment');
  Route::model('loan_disbursed_by', 'App\Models\LoanDisbursedBy');
  Route::model('loan_product', 'App\Models\LoanProduct');
  Route::model('loan_fee', 'App\Models\LoanFee');
  Route::model('repayment', 'App\Models\LoanRepayment');
  Route::model('loan', 'App\Models\Loan');
  Route::model('user', 'App\Models\User');
  Route::model('expense', 'App\Models\Expense');
  Route::model('expense_type', 'App\Models\ExpenseType');
  Route::model('collateral', 'App\Models\Collateral');
  Route::model('collateral_type', 'App\Models\CollateralType');
  Route::model('other_income', 'App\Models\OtherIncome');
  Route::model('other_income_type', 'App\Models\OtherIncomeType');
  Route::model('payroll', 'App\Models\Payroll');
  Route::model('loan_repayment_method', 'App\Models\LoanRepaymentMethod');
  Route::model('permission', 'App\Models\Permission');
  Route::model('loan_application', 'App\Models\LoanApplication');
  Route::model('saving', 'App\Models\Saving');
  Route::model('savings_product', 'App\Models\SavingProduct');
  Route::model('savings_fee', 'App\Models\SavingFee');
  Route::model('savings_transaction', 'App\Models\SavingTransaction');
  Route::model('asset', 'App\Models\Asset');
  Route::model('asset_type', 'App\Models\AssetType');
  Route::model('asset_valuation', 'App\Models\AssetValuation');
  Route::model('capital', 'App\Models\Capital');
  Route::model('guarantor', 'App\Models\Guarantor');
  Route::model('borrower_group', 'App\Models\BorrowerGroup');
  Route::model('provision', 'App\Models\ProvisionRate');
  Route::model('bank', 'App\Models\BankAccount');
  Route::model('branch', 'App\Models\Branch');
  Route::model('sms_gateway', 'App\Models\SmsGateway');
  Route::model('product', 'App\Models\Product');
  Route::model('warehouse', 'App\Models\Warehouse');
  Route::model('product_category', 'App\Models\ProductCategory');
  Route::model('supplier', 'App\Models\Supplier');
  Route::model('product_check_in', 'App\Models\ProductCheckin');
  Route::model('product_check_out', 'App\Models\ProductCheckout');
  Route::model('product_check_in_item', 'App\Models\ProductCheckinItem');
  Route::model('product_check_out_item', 'App\Models\ProductCheckoutItem');
  Route::model('loan_overdue_penalty', 'App\Models\LoanOverduePenalty');
  Route::model('chart_of_account', 'App\Models\ChartOfAccount');
  Route::model('charge', 'App\Models\Charge');
  Route::model('loan_transaction', 'App\Models\LoanTransaction');

require_once __DIR__."/auth.web.php";

require_once __DIR__."/dashboard.web.php";

require_once __DIR__."/borrower.web.php";
//route for custom fields
require_once __DIR__."/custom-fields.web.php";
//route for borrowers
 require_once __DIR__."/borrower.web.php";
//route for guarantors
 require_once __DIR__."/guarantor.web.php";

//route for setting
require_once __DIR__."/setting.web.php";
//route for user
require_once __DIR__."/user.web.php";
//route for loans
require_once __DIR__."/loan.web.php";
//loan repayment list
require_once __DIR__."/repayment.web.php";
//route for tax
require_once __DIR__."/tax.web.php";
//route for payroll
 require_once __DIR__."/payroll.web.php";
//route for expenses
 require_once __DIR__."/expenses.web.php";
//route for other income
 require_once __DIR__."/income.web.php";
//route for collateral
 require_once __DIR__."/collateral.web.php";
//route for reports
require_once __DIR__."/reports.web.php";
//route for communication
require_once __DIR__."/communication.web.php";
//routes for clients
require_once __DIR__."/clients.web.php";
//route for savings
require_once __DIR__."/savings.web.php";
//routes for assets
require_once __DIR__."/assets.web.php";
//route for capital
require_once __DIR__."/capital.web.php";
//routes branches
require_once __DIR__."/branches.web.php";
//routes for sms gateways
require_once __DIR__."/sms.web.php";
//route for suppliers
require_once __DIR__."/supplier.web.php";
//route for warehouses
require_once __DIR__."/warehouses.web.php";
//route for products
require_once __DIR__."/products.web.php";
//routes for check in
require_once __DIR__."/checkin-checkout.web.php";
//route for chart of accounts
require_once __DIR__."/coa.web.php";
require_once __DIR__."/accounting.web.php";
//route for charges
require_once __DIR__."/charge.web.php";

Route::get('audit_trail/data', 'AuditTrailController@index');
