<?php

use App\Http\Controllers\AssetController;
use Illuminate\Support\Facades\Route;

Route::prefix('asset')->group(function () {
  Route::get('data', [AssetController::class, 'index']);
  Route::get('create', [AssetController::class, 'create']);
  Route::post('store', [AssetController::class, 'store']);
  Route::get('{asset}/edit', [AssetController::class, 'edit']);
  Route::get('{asset}/show', [AssetController::class, 'show']);
  Route::post('{id}/update', [AssetController::class, 'update']);
  Route::get('{id}/delete', [AssetController::class, 'delete']);
  Route::get('{id}/delete_file', [AssetController::class, 'deleteFile']);

  //expense types
  Route::get('type/data', [AssetController::class, 'indexType']);
  Route::get('type/create', [AssetController::class, 'createType']);
  Route::post('type/store', [AssetController::class, 'storeType']);
  Route::get('type/{asset_type}/edit', [AssetController::class, 'editType']);
  Route::get('type/{asset_type}/show', [AssetController::class, 'showType']);
  Route::post('type/{id}/update', [AssetController::class, 'updateType']);
  Route::get('type/{id}/delete', [AssetController::class, 'deleteType']);
});
