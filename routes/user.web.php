<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('user')->group( function () {
  Route::get('data', [UserController::class, 'index']);
  Route::get('create', [UserController::class, 'create']);
  Route::post('store', [UserController::class, 'store']);
  Route::get('{user}/edit', [UserController::class, 'edit']);
  Route::get('{user}/show', [UserController::class, 'show']);
  Route::post('{id}/update', [UserController::class, 'update']);
  Route::get('{id}/delete', [UserController::class, 'delete']);
  Route::get('profile', [UserController::class, 'profile']);
  Route::post('profile', [UserController::class, 'profileUpdate']);
  //manage permissions
  Route::get('permission/data', [UserController::class, 'indexPermission']);
  Route::get('permission/create', [UserController::class, 'createPermission']);
  Route::post('permission/store', [UserController::class, 'storePermission']);
  Route::get('permission/{permission}/edit', [UserController::class, 'editPermission']);
  Route::post('permission/{id}/update', [UserController::class, 'updatePermission']);
  Route::get('permission/{id}/delete', [UserController::class, 'deletePermission']);
  //manage roles
  Route::get('role/data', [UserController::class, 'indexRole']);
  Route::get('role/create', [UserController::class, 'createRole']);
  Route::post('role/store', [UserController::class, 'storeRole']);
  Route::get('role/{id}/edit', [UserController::class, 'editRole']);
  Route::post('role/{id}/update', [UserController::class, 'updateRole']);
  Route::get('role/{id}/delete', [UserController::class, 'deleteRole']);
});
