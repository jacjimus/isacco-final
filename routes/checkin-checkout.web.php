<?php

use App\Http\Controllers\ProductCheckinController;
use App\Http\Controllers\ProductCheckoutController;
use Illuminate\Support\Facades\Route;

Route::prefix('check_in')->group(function () {

  Route::get('data', [ProductCheckinController::class, 'index']);
  Route::get('create', [ProductCheckinController::class, 'create']);
  Route::post('store', [ProductCheckinController::class, 'store']);
  Route::get('{product_check_in}/show', [ProductCheckinController::class, 'show']);
  Route::get('{product_check_in}/edit', [ProductCheckinController::class, 'edit']);
  Route::post('{id}/update', [ProductCheckinController::class, 'update']);
  Route::get('{id}/delete', [ProductCheckinController::class, 'delete']);
  Route::get('{product}/get_product_data', [ProductCheckinController::class, 'get_product_data']);
  Route::get('payment/data', [ProductCheckinController::class, 'indexPayment']);

});
//routes for check out
Route::prefix('check_out')->group(function () {

  Route::get('data', [ProductCheckoutController::class, 'index']);
  Route::get('create', [ProductCheckoutController::class, 'create']);
  Route::post('store', [ProductCheckoutController::class, 'store']);
  Route::get('{product_check_out}/show', [ProductCheckoutController::class, 'show']);
  Route::get('{product_check_out}/edit', [ProductCheckoutController::class, 'edit']);
  Route::post('{id}/update', [ProductCheckoutController::class, 'update']);
  Route::get('{id}/delete', [ProductCheckoutController::class, 'delete']);
  Route::any('overview', [ProductCheckoutController::class, 'overview']);

});
