<?php

use App\Http\Controllers\BranchController;
use Illuminate\Support\Facades\Route;

Route::prefix('branch')->group(function () {

  Route::get('data', [BranchController::class, 'index']);
  Route::get('create', [BranchController::class, 'create']);
  Route::post('store', [BranchController::class, 'store']);
  Route::get('{branch}/show', [BranchController::class, 'show']);
  Route::get('{branch}/edit', [BranchController::class, 'edit']);
  Route::post('{id}/update', [BranchController::class, 'update']);
  Route::get('{id}/delete', [BranchController::class, 'delete']);
  Route::get('{id}/delete_file', [BranchController::class, 'deleteFile']);
  Route::post('{id}/add_user', [BranchController::class, 'addUser']);
  Route::get('{id}/remove_user', [BranchController::class, 'removeUser']);
  Route::get('change', [BranchController::class, 'change']);
  Route::post('change', [BranchController::class, 'updateChange']);
});
