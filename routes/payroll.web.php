<?php

use App\Http\Controllers\PayrollController;
use Illuminate\Support\Facades\Route;

Route::prefix('payroll')->group( function () {
  Route::get('data', [PayrollController::class, 'index']);
  Route::get('create', [PayrollController::class, 'create']);
  Route::post('store', [PayrollController::class, 'store']);
  Route::get('{payroll}/show', [PayrollController::class, 'show']);
  Route::get('{payroll}/edit', [PayrollController::class, 'edit']);
  Route::post('{id}/update', [PayrollController::class, 'update']);
  Route::get('{id}/delete', [PayrollController::class, 'delete']);
  Route::get('getUser/{id}', [PayrollController::class, 'getUser']);
  Route::get('{payroll}/payslip', [PayrollController::class, 'pdfPayslip']);
  Route::get('{user}/data', [PayrollController::class, 'staffPayroll']);
//template
  Route::any('template', [PayrollController::class, 'indexTemplate']);
  Route::get('template/{id}/edit', [PayrollController::class, 'editTemplate']);
  Route::post('template/{id}/update', [PayrollController::class, 'updateTemplate']);
  Route::get('template/{id}/delete_meta', [PayrollController::class, 'deleteTemplateMeta']);
  Route::post('template/{id}/add_row', [PayrollController::class, 'addTemplateRow']);
});
