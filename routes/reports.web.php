<?php

use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;

Route::prefix('report')->group(function () {
  Route::any('borrower_report', [ReportController::class, 'borrower_report']);
  Route::any('loan_report', [ReportController::class, 'loan_report']);
  Route::any('financial_report', [ReportController::class, 'financial_report']);
  Route::any('company_report', [ReportController::class, 'company_report']);
  Route::any('savings_report', [ReportController::class, 'savings_report']);
  Route::prefix('financial_report')->group(function () {
    Route::any('trial_balance', [ReportController::class, 'trial_balance']);
    Route::any('trial_balance/pdf', [ReportController::class, 'trial_balance_pdf']);
    Route::any('trial_balance/excel', [ReportController::class, 'trial_balance_excel']);
    Route::any('trial_balance/csv', [ReportController::class, 'trial_balance_csv']);
    Route::any('ledger', [ReportController::class, 'ledger']);
    Route::any('journal', [ReportController::class, 'journal']);
    Route::any('income_statement', [ReportController::class, 'income_statement']);
    Route::any('income_statement/pdf', [ReportController::class, 'income_statement_pdf']);
    Route::any('income_statement/excel', [ReportController::class, 'income_statement_excel']);
    Route::any('income_statement/csv', [ReportController::class, 'income_statement_csv']);
    Route::any('balance_sheet', [ReportController::class, 'balance_sheet']);
    Route::any('balance_sheet/pdf', [ReportController::class, 'balance_sheet_pdf']);
    Route::any('balance_sheet/excel', [ReportController::class, 'balance_sheet_excel']);
    Route::any('balance_sheet/csv', [ReportController::class, 'balance_sheet_csv']);
    Route::any('cash_flow', [ReportController::class, 'cash_flow']);
    Route::any('provisioning', [ReportController::class, 'provisioning']);
    Route::any('provisioning/pdf', [ReportController::class, 'provisioning_pdf']);
    Route::any('provisioning/excel', [ReportController::class, 'provisioning_excel']);
    Route::any('provisioning/csv', [ReportController::class, 'provisioning_csv']);
  });
  Route::prefix('loan_report')->group(function () {
    Route::any('expected_repayments', [ReportController::class, 'expected_repayments']);
    Route::any('expected_repayments/pdf', [ReportController::class, 'expected_repayments_pdf']);
    Route::any('expected_repayments/excel', [ReportController::class, 'expected_repayments_excel']);
    Route::any('expected_repayments/csv', [ReportController::class, 'expected_repayments_csv']);
    Route::any('repayments_report', [ReportController::class, 'repayments_report']);
    Route::any('repayments_report/pdf', [ReportController::class, 'repayments_report_pdf']);
    Route::any('repayments_report/excel', [ReportController::class, 'repayments_report_excel']);
    Route::any('repayments_report/csv', [ReportController::class, 'repayments_report_csv']);
    Route::any('collection_sheet', [ReportController::class, 'collection_sheet']);
    Route::any('collection_sheet/pdf', [ReportController::class, 'collection_sheet_pdf']);
    Route::any('collection_sheet/excel', [ReportController::class, 'collection_sheet_excel']);
    Route::any('collection_sheet/csv', [ReportController::class, 'collection_sheet_csv']);
    Route::any('arrears_report', [ReportController::class, 'arrears_report']);
    Route::any('arrears_report/pdf', [ReportController::class, 'arrears_report_pdf']);
    Route::any('arrears_report/excel', [ReportController::class, 'arrears_report_excel']);
    Route::any('arrears_report/csv', [ReportController::class, 'arrears_report_csv']);
    Route::any('disbursed_loans', [ReportController::class, 'disbursed_loans']);
    Route::any('disbursed_loans/pdf', [ReportController::class, 'disbursed_loans_pdf']);
    Route::any('disbursed_loans/excel', [ReportController::class, 'disbursed_loans_excel']);
    Route::any('disbursed_loans/csv', [ReportController::class, 'disbursed_loans_csv']);
    Route::any('cash_flow', [ReportController::class, 'cash_flow']);

  });
  Route::prefix('borrower_report')->group(function () {
    Route::any('borrower_numbers', [ReportController::class, 'borrower_numbers']);
    Route::any('borrower_numbers/pdf', [ReportController::class, 'borrower_numbers_pdf']);
    Route::any('borrower_numbers/excel', [ReportController::class, 'borrower_numbers_excel']);
    Route::any('borrower_numbers/csv', [ReportController::class, 'borrower_numbers_csv']);
    Route::any('top_borrowers', [ReportController::class, 'top_borrowers']);
    Route::any('top_borrowers/pdf', [ReportController::class, 'top_borrowers_pdf']);
    Route::any('top_borrowers/excel', [ReportController::class, 'top_borrowers_excel']);
    Route::any('top_borrowers/csv', [ReportController::class, 'top_borrowers_csv']);
    Route::any('borrowers_overview', [ReportController::class, 'borrowers_overview']);
    Route::any('borrowers_overview/pdf', [ReportController::class, 'borrowers_overview_pdf']);
    Route::any('borrowers_overview/excel', [ReportController::class, 'borrowers_overview_excel']);
    Route::any('borrowers_overview/csv', [ReportController::class, 'borrowers_overview_csv']);


  });
  Route::prefix('company_report')->group(function () {
    Route::any('products_summary', [ReportController::class, 'products_summary']);
    Route::any('products_summary/pdf', [ReportController::class, 'products_summary_pdf']);
    Route::any('products_summary/excel', [ReportController::class, 'products_summary_excel']);
    Route::any('products_summary/csv', [ReportController::class, 'products_summary_csv']);
    Route::any('general_report', [ReportController::class, 'general_report']);
    Route::any('top_borrowers', [ReportController::class, 'top_borrowers']);
    Route::any('top_borrowers/pdf', [ReportController::class, 'top_borrowers_pdf']);
    Route::any('top_borrowers/excel', [ReportController::class, 'top_borrowers_excel']);
    Route::any('top_borrowers/csv', [ReportController::class, 'top_borrowers_csv']);
    Route::any('borrowers_overview', [ReportController::class, 'borrowers_overview']);
    Route::any('borrowers_overview/pdf', [ReportController::class, 'borrowers_overview_pdf']);
    Route::any('borrowers_overview/excel', [ReportController::class, 'borrowers_overview_excel']);
    Route::any('borrowers_overview/csv', [ReportController::class, 'borrowers_overview_csv']);


  });
  Route::prefix('savings_report')->group(function () {
    Route::any('savings_transactions', [ReportController::class, 'savings_transactions']);
    Route::any('savings_transactions/pdf', [ReportController::class, 'savings_transactions_pdf']);
    Route::any('savings_transactions/excel', [ReportController::class, 'savings_transactions_excel']);
    Route::any('savings_transactions/csv', [ReportController::class, 'savings_transactions_csv']);
    Route::any('savings_balance', [ReportController::class, 'savings_balance']);
    Route::any('savings_balance/pdf', [ReportController::class, 'savings_balance_pdf']);
    Route::any('savings_balance/excel', [ReportController::class, 'savings_balance_excel']);
    Route::any('savings_balance/csv', [ReportController::class, 'savings_balance_csv']);


  });
  Route::any('cash_flow', [ReportController::class, 'cash_flow']);
  Route::any('collection', [ReportController::class, 'collection_report']);
  Route::any('loan_product', [ReportController::class, 'profit_loss']);

  Route::any('loan_list', [ReportController::class, 'loan_list']);
  Route::any('loan_balance', [ReportController::class, 'loan_balance']);
  Route::any('loan_arrears', [ReportController::class, 'loan_arrears']);
  Route::any('loan_transaction', [ReportController::class, 'loan_transaction']);
  Route::any('loan_classification', [ReportController::class, 'loan_classification']);
  Route::any('loan_projection', [ReportController::class, 'loan_projection']);
  //Route::any('borrower_report', [ReportController::class, 'borrower_report']);
  Route::any('collection_sheet', [ReportController::class, 'collection_sheet']);

});
