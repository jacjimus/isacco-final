<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BorrowerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      $gender = $this->faker->randomElement(['male', 'female']);
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'user_id' => 1,
            'gender' => $gender,
            'title' => $this->faker->title(),
            'mobile' => $this->faker->unique()->phoneNumber(),
            'email' => $this->faker->unique()->safeEmail(),
            'id_no' => $this->faker->unique()->randomNumber(),
            'unique_number' => $this->faker->unique()->randomNumber(),
            'dob' => $this->faker->dateTimeBetween('1986-01-01', '2012-12-31')
              ->format('Y-m-d'),
            'address' => $this->faker->address(),
            'city' => 'Kitui',
        ];
    }
}
