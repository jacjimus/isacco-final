<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SavingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'savings_product_id' => '1',
            'notes' => '',
            'date' => now(),
            'month' => '05',
            'account_number' => $this->faker->unique()->randomNumber(),
            'status' => 'active',
            'loan_officer_id' => '',
            'year' => '2022',
        ];
    }
}
