<?php

use App\Models\Borrower;
use App\Models\Saving;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(RolesTableSeeder::class);
        //$this->call(PermissionsTableSeeder::class);
       // $this->call(UsersTableSeeder::class);
        //$this->call(SettingsTableSeeder::class);
        //
      //$this->call(CountryTableSeeder::class);
        //$this->call(PayrollTemplateTableSeeder::class);
      Borrower::truncate();
      Saving::truncate();
        Borrower::factory()->count(100)->create();
        foreach(Borrower::all() AS $borrower) {
          Saving::factory()->create([
          'borrower_id' => $borrower->id,
          ]);
     }
    }
}
